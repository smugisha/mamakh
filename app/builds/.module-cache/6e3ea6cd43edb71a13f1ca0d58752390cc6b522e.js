/** @jsx React.DOM */

/*
  This is cart app that uses React JS
  and localStorage to store the basket
  contents
*/
var resources = require('./resources');
var routes = require('../routes/allRoutes');
var React = require('react');

var AddSub = React.createClass({displayName: 'AddSub',
  getInitialState: function() {
    return { quantity: 0 };
  },
  
  handleAddClick : function(event) {
    var quantity = this.state.quantity + 1;
    this.setState({quantity: quantity});
  },
  
  handleSubClick: function (event) {
    var quantity = this.state.quantity - 1;
    quantity = (quantity < 0) ? 0 : quantity;
    this.setState({quantity: quantity});
  },
  
  handleChange: function(event) {
    var val = event.target.value;
    this.setState({quantity: ((!isInteger(val) || val<0 ) ? 0 : val)});
  },
  
  handleClick: function(event) {
    event.preventDefault();
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          document.getElementsByClassName('total-price')[0].innerHTML=JSON.parse(xhr.responseText).cart.total;
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('post', event.target.href, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify({id: this.props.food._id, quantity: this.state.quantity}));
    }
  },
  
  render: function() {
    return (
      React.DOM.div( {className:"mmk-order-controls"}, 
        React.DOM.div( {className:"mmk-controls"}, 
	  React.DOM.button( {className:"pure-button button-xsmall addsub-button",
                  onClick:this.handleAddClick}, "+"),
          React.DOM.input(
            {className:"mmk-addsub-input",
            type:"text",
            value:this.state.quantity,
            onChange:this.handleChange,
            size:"3",
            maxlength:"3"}
          ),
          React.DOM.button( {className:"button-xsmall pure-button mmk-addsub-button",
                  onClick:this.handleSubClick}, "-")
        ),
	React.DOM.div( {className:"mmk-add-to-cart"}, 
	  React.DOM.a(      {className:"pure-button button-xsmall mamak-button-red",
                  href:routes.cart.addFood, onClick:this.handleClick}, "Add to Cart")
	)
      )
    );
  }
});

var CartIndicator = React.createClass({displayName: 'CartIndicator',
  render: function() {
    return (
      React.DOM.div(null, 
      React.DOM.a( {href:routes.checkout.cart} , 
        React.DOM.div( {className:"cart-view"}, 
          "Basket: ", React.DOM.span( {className:"total-price"}, this.props.totalPrice),React.DOM.br(null)
        )
      )
      )
    );
  }
});

/*
  show JSON data from server cart
  JSON contains foodName and quantity
  and price will be calculated
  together with total
*/

var ShoppingCartTable = React.createClass({displayName: 'ShoppingCartTable',
  getInitialState: function() {
    return { products: {}, total: 0 };
  },
  
  componentDidMount: function() {
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          this.setState({ products: JSON.parse(xhr.responseText).cart.products,
                          total: JSON.parse(xhr.responseText).cart.total});
          console.log(xhr.responseText);
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('post', routes.cart.addFood, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send();
      }
  },
  
  removeClick: function(event) {
    event.preventDefault();
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          this.setState({ products: JSON.parse(xhr.responseText).cart.products,
                          total: JSON.parse(xhr.responseText).cart.total});
          document.getElementsByClassName('total-price')[0].innerHTML=JSON.parse(xhr.responseText).cart.total;
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('delete', event.target.href, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify({id: event.target.id}));
    }
  },
  
  render: function() {
    var items = {};
    var total = 0;
    if (this.state.products) {
      items = this.state.products;
      total = this.state.total;
    }
    if (this.props.cart) {
      items = (this.props.cart.products) ? this.props.cart.products : {};
      total = (this.props.cart.total) ? this.props.cart.total : 0;
    }
    var removeClick = this.removeClick;
    console.log(JSON.stringify(items));
    var shoppingCart = Object.keys(items).map(function(id, index){
      return (
        React.DOM.tr( {className:"sc-body"}, 
        React.DOM.td( {className:"sc sc-name sc-body"}, 
        React.DOM.a( {href:routes.cart.removeFood,
        onClick:removeClick,
        className:"pure-button",
        id:id}, "x"),
        items[id].name
        ),
        React.DOM.td( {className:"sc sc-qty sc-body"}, 
        items[id].quantity
        ),
        React.DOM.td( {className:"sc sc-price sc-body"}, 
        items[id].price.toFixed(2)
         )
         )
        );
    });
    console.log('checkout ->',this.props.checkoutLink);
    return (
      React.DOM.div( {className:"content"}, 
        React.DOM.table( {className:"cart-reciept sc-container"}, 
          /* header */
          React.DOM.tr( {className:"sc-header"} , 
            React.DOM.th( {className:"sc-name"}, "Item"),
            React.DOM.th( {className:"sc-qty"}, "Qty"),
            React.DOM.th( {className:"sc-price"}, "Price")
          ),
          shoppingCart
          ),
            React.DOM.div( {className:"sc-total " }, 
              React.DOM.hr(null ),
              React.DOM.span( {className:"sc-total-text"}, "Total:"),
              React.DOM.span( {className:"sc-total-value"}, parseFloat(total).toFixed(2))
            )
          )
      );
/*
if (isEmpty(items)) {
return (
<div className='content'>
<h1>Your cart is empty</h1>
<p>Add items to order stuff.</p>
<hr />
<div className="sc-checkout">
<a className='pure-button'
href={routes.home}>Make an Order</a>
</div>
</div>
);
}*/
  }
});

var ShoppingCart = React.createClass({displayName: 'ShoppingCart',
  render : function() {
    var Menu = require('./index').Menu;
    console.log(this.props.chekoutLink);
    return (
      React.DOM.html(null, 
      React.DOM.head(null, 
        React.DOM.title(null, "mamak"),
        React.DOM.link( {rel:"stylesheet", href:resources.pureCss}),
        React.DOM.link( {rel:"stylesheet", href:resources.mamakStyle}),
        React.DOM.link( {rel:"stylesheet", href:resources.fontAwesome})
      ),
      React.DOM.body(null, 
      React.DOM.div( {id:"layout"}, 
        Menu( {totalPrice:this.props.totalPrice}),
      React.DOM.div( {id:"cart"}, 
        React.DOM.div( {id:"main"}, 
        React.DOM.div( {className:"header"}, 
          React.DOM.h1(null, "Your selection")
        ),
        React.DOM.div( {className:"content"}, 
        React.DOM.div( {className:"shopping-cart-table"}, 
          ShoppingCartTable(
            {cart:this.props.cart}
          )
        ),
        React.DOM.div( {className:"content sc-checkout"}, 
	      React.DOM.a( {className:"pure-button", href:this.props.checkoutLink}, 
	        React.DOM.i( {className:"fa fa-shopping-cart fa-lg"}),
	           "Checkout"
	      )
        )
        ), " " /* content */
        ), " " /* main */
      )
      ),
        React.DOM.script( {src:resources.mamakJS}),      
        React.DOM.script( {src:resources.fbReactLibJS}),
        React.DOM.script( {src:resources.mamakApp})
      )
      )
    );
  },
});

// utilities from SO
// http://stackoverflow.com/a/6726556
function isNumber(value) {
  if ((undefined === value) || (null === value)) {
    return false;
  }
  if (typeof value == 'number') {
    return true;
  }
  return !isNaN(value - 0);
}

function isInteger(value) {
  if ((undefined === value) || (null === value)) {
    return false;
  }
  return value % 1 == 0;
}

// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

module.exports.ShoppingCart = ShoppingCart;
module.exports.AddSub = AddSub;
module.exports.CartIndicator = CartIndicator;
module.exports.ShoppingCartTable = ShoppingCartTable;
