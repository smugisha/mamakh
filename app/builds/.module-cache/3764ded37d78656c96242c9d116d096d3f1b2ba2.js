/** @jsx React.DOM */

// guest checkout
// contains guest checkout form
// contains routes to sign in
// contains routes to register
// contains checkout order button
var React = require('react');

// guest checkout form
var guestForm = React.createClass({displayName: 'guestForm',
  getInitialState: function() {
    return ( {inputValid: true, submitValid: true});
  },
  
  handleValidate: function () {
    var x = event.target.value;
  },

  render: function() {
    var value = (this.props.loggedIn) ?
      this.props.user : 'guest';
    var style = {
      backgroundColor: (this.state.inputValid) ? 'white' : 'red'
    };
    
    return (
      React.DOM.div(null, 
      React.DOM.form( {name:"checkout", method:"post", action:this.props.action} , 
        React.DOM.div(null, 
          React.DOM.label( {htmlFor:"name"}, "Name:"),
          React.DOM.input(
            {type:"name",
            id:"name",
            style:this.divStyle,
            onBlur:this.handleValidate,
            required:true}
          )
        ),
        React.DOM.div(null, 
          React.DOM.label( {htmlFor:"phoneNumber"}, "Phone Number:"),
          React.DOM.input(
            {type:"phoneNumber",
            name:"phoneNumber",
            style:this.divStyle,
            onBlur:this.handleValidate,
            required:true}
          )
        ),
        React.DOM.div(null, 
          React.DOM.label( {htmlFor:"address"}, "Address: " ),
          React.DOM.textarea(
            {name:"address",
            id:"address",
            required:true}
          )
        ),
      
        React.DOM.div(null, 
          React.DOM.input( {type:"submit", value:'Checkout as ' + value} )
        ),
        React.DOM.script( {src:"/javascripts/guestForm.js"})
      )
      )
    );
  }
});

module.exports.guestForm = guestForm;
