/**
 * @jsx React.DOM
 */

var React = require('react');
var AddSub = require('./cart').AddSub;

/* Food Row:
 *  displays a row of a particular food item
 */
var FoodRow = React.createClass({displayName: 'FoodRow',
  render: function() {
    var name = this.props.food.restaurantOpen ?
      this.props.food.name :
      React.DOM.span( {style:{color: 'red'}, className:"name"}, 
        this.props.food.name
      );

    var description = 
        React.DOM.section( {className:"mmk-food-description"}, 
          name,React.DOM.br(null ),
          React.DOM.span( {className:"mmk-food-restaurantName"}, this.props.food.restaurantName),React.DOM.br(null ),
          React.DOM.span( {className:"mmk-food-description"}, this.props.food.description)
        );

    return (
      React.DOM.div( {className:"mmk-food"}, 
	React.DOM.div( {className:"mmk-food-img"}, 
          React.DOM.img( {src:this.props.food.imgUrl,
               width:"150px", height:"150px",
	       className:"mmk-food-img"})
	),
        description,
	React.DOM.div( {className:"mmk-order"}, 
	    React.DOM.div( {className:"mmk-food-price"}, "RM ", this.props.food.price.toFixed(2)),
            AddSub( {food:this.props.food})
        )
      )
    );      
  }
});




/* Food Table
 * diplays the foods one by one
 */
var FoodTable = React.createClass({displayName: 'FoodTable',
  render: function() {
    var filterText = this.props.filterText;
    var foodRows = this.props.food.map(function (food) {
      if (food.name.indexOf(filterText) === -1) {
        return;
      }
      else {
        return ( 
            FoodRow(
              {food:food,
              key:food.name}
            )
        );
      }
    });
    
    return (
      React.DOM.div(null, 
        React.DOM.div( {className:"food-list"}, 
          foodRows
        )
      )
    );
  }
});



/* Search Bar
 * allows typing to search for food items
 */

var SearchBar = React.createClass({displayName: 'SearchBar',
  handleChange: function() {
    this.props.onUserInput(
      this.refs.filterTextInput.getDOMNode().value
    );
  },
  
  render: function() {
    return (
      React.DOM.form( {onSubmit:this.handleSubmit}, 
      React.DOM.h2(null, 
        React.DOM.input(
          {className:"pure-input mmk-search-input",
          type:"text",
          placeholder:"Search...",
          value:this.props.filterText,
          ref:"filterTextInput",
          onChange:this.handleChange}
        )
      )
      )
    );
  }
});



/* FilterableFoodTable
 *
 */
var FilterableFoodTable = React.createClass({displayName: 'FilterableFoodTable',
  getInitialState: function() {
    return {
      filterText: '',
      data: []
    };
  },
  
  handleUserInput: function(filterText) {
    this.setState({
      filterText: filterText
    });
    
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          this.setState({data: JSON.parse(xhr.responseText)});
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('get', 'search?q='+filterText, true);
      xhr.send();
    } else {
      // server-side rendering
    }
  },
  
  render: function() {
    console.log(this.state.data);
    return (
      React.DOM.div( {className:"food-table"}, 
        React.DOM.div( {className:"header"}, 
        SearchBar(
          {filterText:this.state.filterText,
          onUserInput:this.handleUserInput}
        )),
        React.DOM.div( {className:"content"}, 
        FoodTable(
          {food:this.state.data,
          filterText:this.state.filterText}
        ))
      )
    );
  }
});


module.exports.FilterableFoodTable = FilterableFoodTable;
