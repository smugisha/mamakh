/** @jsx React.DOM */

var React = require('react');
var FilterableFoodTable = require('./mamakFoodTable').FilterableFoodTable;
var CartIndicator = require('./cart').CartIndicator;
var routes = require('../routes/allRoutes');
var resources = require('./resource');

var Menu = React.createClass({displayName: 'Menu',
  render: function() {
    return (
    React.DOM.div(null, 
    React.DOM.a( {href:"#menu", id:"menuLink", className:"menu-link"}, 
        React.DOM.span(null)
    ),

    React.DOM.div( {id:"menu"}, 
        React.DOM.div( {className:"pure-menu pure-menu-open"}, 
            React.DOM.a( {className:"pure-menu-heading",
               href:routes.home}, "mamak"),
            React.DOM.ul(null, 
                React.DOM.li(null, React.DOM.a( {href:routes.user.login}, "Login")),
                React.DOM.li(null, React.DOM.a( {href:routes.user.logout}, "Logout"))
            ),
                CartIndicator( {totalPrice:this.props.totalPrice} )
        )
    )
    )
    );
  }
});

var Index = React.createClass({displayName: 'Index',
  
  render: function() {
    
    return (
      React.DOM.html(null, 
      React.DOM.head(null, 
        React.DOM.title(null, "mamak"),
        React.DOM.link( {rel:"stylesheet", href:resources.pureCss}),
        React.DOM.link( {rel:"stylesheet", href:resources.mamakStyle})
      ),
      React.DOM.body(null, 
      React.DOM.div( {id:"layout"}, 
        Menu( {totalPrice:this.props.totalPrice}),
      React.DOM.div( {id:"main"}, 
        FilterableFoodTable(null )
      )
      ),
        React.DOM.script( {src:resources.mamakJS}),      
        React.DOM.script( {src:resources.fbReactLibJS}),
        React.DOM.script( {src:resources.mamakApp})
      )
      )
    );
  }
});

module.exports.Index = Index;
module.exports.Menu = Menu;
