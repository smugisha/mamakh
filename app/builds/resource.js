module.exports = {
  pureCss      : 'stylesheets/pure-min.css',
  mamakStyle   : 'stylesheets/ui.css',
  mamakJS      : 'javascripts/1.js',
  fbReactLibJS : '//fb.me/react-0.10.0.min.js',
  mamakApp     :'javascripts/bundle.js',
};