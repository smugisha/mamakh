module.exports = {
  root: '../views',
  checkout : function (view) {
    return {
      cart    : this.root + '/cart' + view,
      order: this.root + '/order' + view,
    };
  },
  
};