/** @jsx React.DOM */

/* main.js
 *  This file is used by browserify to instantiate the app
 */

var React = require('react');
var FilterableFoodTable = require('./mamakFoodTable').FilterableFoodTable;
var Cart = require('./cart').ShoppingCartTable;

if (document.getElementsByClassName('shopping-cart-table')[0]) {
React.renderComponent(Cart(null) , document.getElementsByClassName('shopping-cart-table')[0]);
}

if (document.getElementsByClassName('food-table')[0]) {
React.renderComponent(FilterableFoodTable(null),
                      document.getElementsByClassName('food-table')[0]);
}
