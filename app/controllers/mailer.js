'use strict'

var nodemailer = require('nodemailer');
var path = require('path');
var emailTemplates = require('email-templates');
var templateDir = path.resolve(__dirname, '..', 'mail-templates');

function createTransport() {
  return nodemailer.createTransport('SMTP', {
    service: 'Gmail',
    auth: {
      user: 'mamakmailer@gmail.com',
      pass: "CKMAILALBVOX@$34"
    }
  });
};

exports.send = function (order, customer) {
  
  emailTemplates(templateDir, function (err, template) {
    if (err) {
      console.log(err);
    } else {
      
      // Prepare node mailer transport object
      var transport = createTransport();
      var locals = {
        email: order.email,
        order: {
          list: order.orders,
          customer: customer,
        },
      };
            
      // Send a single email
      template('order', locals, function(err, html, text) {
        if (err) {
          console.log(err);
        } else {
          transport.sendMail({
            from: customer.username,
            to: locals.email,
            subject: 'Mamak order for: ' + customer.username,
            html: html,
            generateTextFromHTML: true
          }, process.nextTick( function(err, responseStatus) {
            console.log('sending email');
            if (err) {
              console.log(err);
            } else {
              if (responseStatus){
                console.log(responseStatus.message);
              }
            }
          }));
        }
      });
    }
  });
}
