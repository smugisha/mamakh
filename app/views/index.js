/** @jsx React.DOM */

var React = require('react');
var FilterableFoodTable = require('./mamakFoodTable').FilterableFoodTable;
var CartIndicator = require('./cart').CartIndicator;
var routes = require('../routes/allRoutes');
var resources = require('./resource');

var Menu = React.createClass({
  render: function() {
    return (
    <div>
    <a href="#menu" id="menuLink" className="menu-link">
        <span></span>
    </a>

    <div id="menu">
        <div className="pure-menu pure-menu-open">
            <a className="pure-menu-heading"
               href={routes.home}>mamak</a>
            <ul>
                <li><a href={routes.user.login}>Login</a></li>
                <li><a href={routes.user.logout}>Logout</a></li>
            </ul>
                <CartIndicator totalPrice={this.props.totalPrice} />
        </div>
    </div>
    </div>
    );
  }
});

var Index = React.createClass({
  
  render: function() {
    
    return (
      <html>
      <head>
        <title>mamak</title>
        <link rel="stylesheet" href={resources.pureCss}></link>
        <link rel="stylesheet" href={resources.mamakStyle}></link>
      </head>
      <body>
      <div id='layout'>
        <Menu totalPrice={this.props.totalPrice}/>
      <div id='main'>
        <FilterableFoodTable />
      </div>
      </div>
        <script src={resources.mamakJS}></script>      
        <script src={resources.fbReactLibJS}></script>
        <script src={resources.mamakApp}></script>
      </body>
      </html>
    );
  }
});

module.exports.Index = Index;
module.exports.Menu = Menu;
