/** @jsx React.DOM */

// guest checkout
// contains guest checkout form
// contains routes to sign in
// contains routes to register
// contains checkout order button
var React = require('react');

// guest checkout form
var guestForm = React.createClass({
  getInitialState: function() {
    return ( {inputValid: true, submitValid: true});
  },
  
  handleValidate: function () {
    var x = event.target.value;
  },

  render: function() {
    var value = (this.props.loggedIn) ?
      this.props.user : 'guest';
    var style = {
      backgroundColor: (this.state.inputValid) ? 'white' : 'red'
    };
    
    return (
      <div>
      <form name='checkout' method='post' action={this.props.action} >
        <div>
          <label htmlFor='name'>Name:</label>
          <input
            type='name'
            id='name'
            style={this.divStyle}
            onBlur={this.handleValidate}
            required
          />
        </div>
        <div>
          <label htmlFor='phoneNumber'>Phone Number:</label>
          <input
            type='phoneNumber'
            name='phoneNumber'
            style={this.divStyle}
            onBlur={this.handleValidate}
            required
          />
        </div>
        <div>
          <label htmlFor='address'>Address: </label>
          <textarea
            name='address'
            id='address'
            required
          ></textarea>
        </div>
      
        <div>
          <input type='submit' value={'Checkout as ' + value} />
        </div>
        <script src='/javascripts/guestForm.js'></script>
      </form>
      </div>
    );
  }
});

module.exports.guestForm = guestForm;
