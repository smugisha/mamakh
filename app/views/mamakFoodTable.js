/**
 * @jsx React.DOM
 */

var React = require('react');
var AddSub = require('./cart').AddSub;

/* Food Row:
 *  displays a row of a particular food item
 */
var FoodRow = React.createClass({
  render: function() {
    var name = this.props.food.restaurantOpen ?
      this.props.food.name :
      <span style={{color: 'red'}} className='name'>
        {this.props.food.name}
      </span>;

    var description = 
        <section className='mmk-food-description'>
          {name}<br />
          <span className='mmk-food-restaurantName'>{this.props.food.restaurantName}</span><br />
          <span className='mmk-food-description'>{this.props.food.description}</span>
        </section>;

    return (
      <div className='mmk-food'>
	<div className='mmk-food-img'>
          <img src={this.props.food.imgUrl}
               width='150px' height='150px'
	       className='mmk-food-img'></img>
	</div>
        {description}
	<div className='mmk-order'>
	    <div className='mmk-food-price'>RM {this.props.food.price.toFixed(2)}</div>
            <AddSub food={this.props.food}/>
        </div>
      </div>
    );      
  }
});




/* Food Table
 * diplays the foods one by one
 */
var FoodTable = React.createClass({
  render: function() {
    var filterText = this.props.filterText;
    var foodRows = this.props.food.map(function (food) {
      if (food.name.indexOf(filterText) === -1) {
        return;
      }
      else {
        return ( 
            <FoodRow
              food={food}
              key={food.name}
            />
        );
      }
    });
    
    return (
      <div>
        <div className='food-list'>
          {foodRows}
        </div>
      </div>
    );
  }
});



/* Search Bar
 * allows typing to search for food items
 */

var SearchBar = React.createClass({
  handleChange: function() {
    this.props.onUserInput(
      this.refs.filterTextInput.getDOMNode().value
    );
  },
  
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
      <h2>
        <input
          className='pure-input mmk-search-input'
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          ref="filterTextInput"
          onChange={this.handleChange}
        />
      </h2>
      </form>
    );
  }
});



/* FilterableFoodTable
 *
 */
var FilterableFoodTable = React.createClass({
  getInitialState: function() {
    return {
      filterText: '',
      data: []
    };
  },
  
  handleUserInput: function(filterText) {
    this.setState({
      filterText: filterText
    });
    
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          this.setState({data: JSON.parse(xhr.responseText)});
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('get', 'search?q='+filterText, true);
      xhr.send();
    } else {
      // server-side rendering
    }
  },
  
  render: function() {
    console.log(this.state.data);
    return (
      <div className='food-table'>
        <div className='header'>
        <SearchBar
          filterText={this.state.filterText}
          onUserInput={this.handleUserInput}
        /></div>
        <div className='content'>
        <FoodTable
          food={this.state.data}
          filterText={this.state.filterText}
        /></div>
      </div>
    );
  }
});


module.exports.FilterableFoodTable = FilterableFoodTable;
