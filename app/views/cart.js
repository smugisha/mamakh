/** @jsx React.DOM */

/*
  This is cart app that uses React JS
  and localStorage to store the basket
  contents
*/
var resources = require('./resources');
var routes = require('../routes/allRoutes');
var React = require('react');

var AddSub = React.createClass({
  getInitialState: function() {
    return { quantity: 0 };
  },
  
  handleAddClick : function(event) {
    var quantity = this.state.quantity + 1;
    this.setState({quantity: quantity});
  },
  
  handleSubClick: function (event) {
    var quantity = this.state.quantity - 1;
    quantity = (quantity < 0) ? 0 : quantity;
    this.setState({quantity: quantity});
  },
  
  handleChange: function(event) {
    var val = event.target.value;
    this.setState({quantity: ((!isInteger(val) || val<0 ) ? 0 : val)});
  },
  
  handleClick: function(event) {
    event.preventDefault();
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          document.getElementsByClassName('total-price')[0].innerHTML=JSON.parse(xhr.responseText).cart.total;
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('post', event.target.href, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify({id: this.props.food._id, quantity: this.state.quantity}));
    }
  },
  
  render: function() {
    return (
      <div className='mmk-order-controls'>
        <div className='mmk-controls'>
	  <button className='pure-button button-xsmall addsub-button'
                  onClick={this.handleAddClick}>+</button>
          <input
            className='mmk-addsub-input'
            type='text'
            value={this.state.quantity}
            onChange={this.handleChange}
            size='3'
            maxlength='3'
          />
          <button className='button-xsmall pure-button mmk-addsub-button'
                  onClick={this.handleSubClick}>-</button>
        </div>
	<div className='mmk-add-to-cart'>
	  <a      className='pure-button button-xsmall mamak-button-red'
                  href={routes.cart.addFood} onClick={this.handleClick}>Add to Cart</a>
	</div>
      </div>
    );
  }
});

var CartIndicator = React.createClass({
  render: function() {
    return (
      <div>
      <a href={routes.checkout.cart} >
        <div className='cart-view'>
          Basket: <span className='total-price'>{this.props.totalPrice}</span><br/>
        </div>
      </a>
      </div>
    );
  }
});

/*
  show JSON data from server cart
  JSON contains foodName and quantity
  and price will be calculated
  together with total
*/

var ShoppingCartTable = React.createClass({
  getInitialState: function() {
    return { products: {}, total: 0 };
  },
  
  componentDidMount: function() {
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          this.setState({ products: JSON.parse(xhr.responseText).cart.products,
                          total: JSON.parse(xhr.responseText).cart.total});
          console.log(xhr.responseText);
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('post', routes.cart.addFood, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send();
      }
  },
  
  removeClick: function(event) {
    event.preventDefault();
    var xhr;
    if (typeof window != 'undefined') {
      if (window.XMLHttpRequest) {
        // client-side rendering
        // code for IE7+, FF, Chrome, Safari, Opera
        xhr = new XMLHttpRequest();
      } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
      }
      // XMLHTTPRequest action
      function stateChange() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
          this.setState({ products: JSON.parse(xhr.responseText).cart.products,
                          total: JSON.parse(xhr.responseText).cart.total});
          document.getElementsByClassName('total-price')[0].innerHTML=JSON.parse(xhr.responseText).cart.total;
        }
      }
      xhr.onreadystatechange = stateChange.bind(this);
      // Sending request to server
      xhr.open('delete', event.target.href, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send(JSON.stringify({id: event.target.id}));
    }
  },
  
  render: function() {
    var items = {};
    var total = 0;
    if (this.state.products) {
      items = this.state.products;
      total = this.state.total;
    }
    if (this.props.cart) {
      items = (this.props.cart.products) ? this.props.cart.products : {};
      total = (this.props.cart.total) ? this.props.cart.total : 0;
    }
    var removeClick = this.removeClick;
    console.log(JSON.stringify(items));
    var shoppingCart = Object.keys(items).map(function(id, index){
      return (
        <tr className="sc-body">
        <td className="sc sc-name sc-body">
        <a href={routes.cart.removeFood}
        onClick={removeClick}
        className='pure-button'
        id={id}>x</a>
        {items[id].name}
        </td>
        <td className="sc sc-qty sc-body">
        {items[id].quantity}
        </td>
        <td className="sc sc-price sc-body">
        {items[id].price.toFixed(2)}
         </td>
         </tr>
        );
    });
    console.log('checkout ->',this.props.checkoutLink);
    return (
      <div className='content'>
        <table className='cart-reciept sc-container'>
          {/* header */}
          <tr className="sc-header" >
            <th className="sc-name">Item</th>
            <th className="sc-qty">Qty</th>
            <th className="sc-price">Price</th>
          </tr>
          {shoppingCart}
          </table>
            <div className="sc-total ">
              <hr />
              <span className="sc-total-text">Total:</span>
              <span className="sc-total-value">{parseFloat(total).toFixed(2)}</span>
            </div>
          </div>
      );
/*
if (isEmpty(items)) {
return (
<div className='content'>
<h1>Your cart is empty</h1>
<p>Add items to order stuff.</p>
<hr />
<div className="sc-checkout">
<a className='pure-button'
href={routes.home}>Make an Order</a>
</div>
</div>
);
}*/
  }
});

var ShoppingCart = React.createClass({
  render : function() {
    var Menu = require('./index').Menu;
    console.log(this.props.chekoutLink);
    return (
      <html>
      <head>
        <title>mamak</title>
        <link rel="stylesheet" href={resources.pureCss}></link>
        <link rel="stylesheet" href={resources.mamakStyle}></link>
        <link rel="stylesheet" href={resources.fontAwesome}></link>
      </head>
      <body>
      <div id='layout'>
        <Menu totalPrice={this.props.totalPrice}/>
      <div id='cart'>
        <div id='main'>
        <div className='header'>
          <h1>Your selection</h1>
        </div>
        <div className='content'>
        <div className='shopping-cart-table'>
          <ShoppingCartTable
            cart={this.props.cart}
          />
        </div>
        <div className='content sc-checkout'>
	      <a className="pure-button" href={this.props.checkoutLink}>
	        <i className="fa fa-shopping-cart fa-lg"></i>
	           Checkout
	      </a>
        </div>
        </div> {/* content */}
        </div> {/* main */}
      </div>
      </div>
        <script src={resources.mamakJS}></script>      
        <script src={resources.fbReactLibJS}></script>
        <script src={resources.mamakApp}></script>
      </body>
      </html>
    );
  },
});

// utilities from SO
// http://stackoverflow.com/a/6726556
function isNumber(value) {
  if ((undefined === value) || (null === value)) {
    return false;
  }
  if (typeof value == 'number') {
    return true;
  }
  return !isNaN(value - 0);
}

function isInteger(value) {
  if ((undefined === value) || (null === value)) {
    return false;
  }
  return value % 1 == 0;
}

// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

module.exports.ShoppingCart = ShoppingCart;
module.exports.AddSub = AddSub;
module.exports.CartIndicator = CartIndicator;
module.exports.ShoppingCartTable = ShoppingCartTable;
