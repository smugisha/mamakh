/** @jsx React.DOM */
var React = require('react');

/* Shopping basket
    - includes add button
    - reciept view
    - shopping cart view (displays the total prices)
    - make order button (checkout button)
    */

/* all buttons */
var buttons = {
  
  /* orderButton
    displayed in checkout form
    sends the order as a post request to the server
    */
  order : function(){
    React.createClass({
      handleUserInput : function() {
        var currentItems = JSON.parse(localStorage.getItem('basket'));
        if (typeof window != 'undefined') {
          if (window.XMLHttpRequest) {
            // client-side rendering
            // code for IE7+, FF, Chrome, Safari, Opera
            xhr = new XMLHttpRequest();
          } else {
            xhr = new ActiveXObject('Microsoft.XMLHTTP');
          }

          // XMLHTTPRequest action
          function stateChange() {
            if (xhr.readyState == 4 && xhr.status == 200) {
              console.log(xhr.responseText);
              this.setState({data: JSON.parse(xhr.responseText)});
            }
          }
          xhr.onreadystatechange = stateChange.bind(this);
          // Sending request to server
          xhr.open('post', '/order', true);
          xhr.send(JSON.stringify(currentItems));
        } else {
          // server-side or no DOM
        }
      }
    });
  },
  
  /* add item button
    displayed in every food item with name add
    */
  add   : function() {
    React.createClass({
    
    });
  }
};

/* reciept view
    display the shopping cart and items
    all items come from the server after sending order request
    */
var reciept = React.createClass({
  
});

/* shopping cart */