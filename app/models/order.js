var arango = require('arango');
var dbUrl = require('../../config/database').collections('orders');
var db = new arango.Connection(dbUrl);

module.exports = {
  
  // add order to database
  submit: function(order, cb) {
    db.document.create('orders', order, function(err, res) {
      if (err) { console.log('err: ', err); }
      cb (err, res);
    });
  },
  
};