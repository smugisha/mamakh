var arango = require('arango');
var dbUrl  = require('../../config/database').collections('foods');
var db = new arango.Connection(dbUrl);

module.exports = {
  
  // search for food
  search: function(foodname, cb) {
    process.nextTick(function() {
      db.query.string =
        'FOR food IN foods '
        + 'FILTER CONTAINS(food.name, @foodname) '
        + 'LIMIT 25 '
        + 'RETURN food';
      db.query.exec({'foodname': foodname}, function(err, res) {
        cb(err, res);
      });
    });
  }
  
};