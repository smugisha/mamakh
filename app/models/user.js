// app/models/user.js
// load the user

var scrypt = require('scrypt');
var scryptParameters = scrypt.params(0.1);

var arango = require('arango');
var dbUrl  = require('../../config/database').collections('users');
var db = new arango.Connection(dbUrl);

var user = {

  create: function(username, password, cb) {
    // create new user and save into database
    var profile = { 'username': username };
    generatePassword(new Buffer(password, 'base64'), function (passwordHash){
      profile.password     = passwordHash;
      profile.creationDate = Date();
      db.document.create('users', profile, function(err, res){
        cb(err, res);
        if (err) { console.log('err: %j', err); }
      });
    });
  },

  findById: function(docid, cb) {
    // find user by _id of the document
    db.document.get(docid,function(err,res){
      if(err) { console.log("err: %j", res); }
      cb(err, res);
      return;
    });
  },

  findOne: function(options, cb){
    // find only one user from the criteria given in options
    // if no options get everything about the user
    var queries = [];
    for (var option in options){
      if (options.hasOwnProperty(option)){
        queries.push('user.'+option+' == "'+options[option]+'"');
      }
    }

    var queryString = queries.join(' && ');

    db.query.string = 'FOR user in users FILTER ' + queryString + ' LIMIT 1 RETURN user';
    db.query.exec(function(err, ret){
      if (err) {
        console.log('db err(%j):', err, ret);
      }
      cb(err, ret);
    });
  },

  verifyPassword: function(hash, password, cb) {
    process.nextTick(function() {
      scrypt.verify.config.hashEncoding = 'base64';
      scrypt.verify.config.keyEncoding  = 'base64';
      scrypt.verify(new Buffer(hash, 'base64'), new Buffer(password, 'base64'), function(err, res) {
        if (err) { console.log (err); }
        cb(res);
      });
    });
  },

  update: function (id, rev, data, cb) {
    return db.document.patch(id, data,
                             { 'rev': rev, 'forceUpdate': 'true' },
                             function(err, res) {
                               if (err) { console.log("err: ", err); }
                               console.log("res: ", res);
                               cb (err, res);
                             });
  },
  
  delete: function(id, data, cb) {
    
  },
  
};

// Generate password for the user asynchronously
function generatePassword(key, cb) {
  scrypt.hash.config.keyEncoding    = 'base64';
  scrypt.hash.config.outputEncoding = 'base64';
  scrypt.hash(key, scryptParameters, function(err, hash) {
    if (err) { console.log (err); }
    cb(hash);  // to do retun err
  });
}

module.exports = user;
