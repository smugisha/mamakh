var food = require('../models/food');

module.exports = {
  search: function(req, res) {
    food.search(req.query.q, function(err, foodlist) {
      res.send(foodlist.result); // json food list from database
    });
  },
  
  // display item
  item: function(req, res) {
  },
};