// app/routes/routes.js
var food     = require('./food');
var cart     = require('./cart');
var checkout = require('./checkout');
var user     = require('./userHandler');
var routes   = require('./allRoutes');

module.exports = function(app, passport) {

  // home page (with login links)
  app.get(routes.home, function(req, res) {
    var React  = require('react');
    var page   = require('../builds/index').Index;
    var totalPrice  = (req.session.cart) ? req.session.cart.total : 0;
    var webapp = React.renderComponentToStaticMarkup(page({totalPrice: totalPrice}));
    res.send(webapp);
  });

  app.get  (routes.user.login,  user.getLogin);
  app.get  (routes.user.signup, user.getSignup);
  app.post (routes.user.signup, user.postSignup);
  app.post (routes.user.login,  user.postLogin);
  app.get  (routes.user.logout, user.logout);
  
  // profile section
  // user has to be logged in to visit
  app.get  (routes.user.profile, user.profile);
  app.post (routes.user.address, user.updateAddress);
  
  // search food
  app.get (routes.products.search, food.search);
  
  // food cart
  // make an order for food using post data JSON
  app.post  (routes.cart.addFood, cart.addProduct);
  app.delete(routes.cart.removeFood, cart.removeProduct);
  app.post  (routes.cart.updateCart, cart.updateCart);
  
  // Checkout order
  app.get  (routes.checkout.cart, checkout.getCart);
//  app.get  (routes.user.address, checkout.getGuest);
//  app.post (routes.user.address, checkout.postGuest);
  app.get  (routes.checkout.order, checkout.getCheckout);
  app.post (routes.checkout.order, checkout.postCheckout);
};

// route middleware to make sure a user is logged in
function isLoggedIn(newRoute){
  return function(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
      return next();

    // if the user is not authenticated
    res.redirect(newRoute);
  }
}

module.exports.isLoggedIn = isLoggedIn;