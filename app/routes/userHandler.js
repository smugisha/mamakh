var passport    = require('passport');
var renderPaint = require('../controllers/renderpaint');
var nodemailer  = require('nodemailer');
var routes      = require('./allRoutes');
var User        = require('../models/user');

module.exports = {
  getLogin: function(req, res) {
    renderPaint(req, res, 'login.ejs',
                { message: req.flash('loginMessage'),
                  action_url: req.url,
                });
  },
  
  postLogin: function(req, res, next) {
    passport.authenticate('local-login', function(err, user, info) {
      if (err)   { return next(err); }
      if (!user) { return res.redirect(req.url); }
      req.login(user, function (err) {
        if (err) { return next(err); }
        return res.redirect(req.query.redirect_url || routes.home);
      });
    })(req, res, next);
  },
  
  getSignup: function(req, res) {
    renderPaint(req, res, 'signup.ejs',
                { message: req.flash('signupMessage'),
                  action_url: req.url,
                });
  },
  
  postSignup: function(req, res, next) {
    passport.authenticate('local-signup', function(err, user, info) {
      if (err)   { return next(err); }
      if (!user) { return res.redirect(req.url); }
      req.login(user, function (err) {
        if (err) { return next(err); }
        return res.redirect(req.query.redirect_url || '/profile/'+ user._id);
      });
    })(req, res, next);
  },
  
  logout: function(req, res) {
    req.logout();
    res.redirect(routes.home);
  },
  
  profile: function(req, res) {
    if (req.isAuthenticated()) {
      if (req.user._id == 'users/'+req.params.userID) {
        renderPaint(req, res, 'profile.ejs', {
          user: req.user,
        });
      } else {
        res.redirect(routes.home);
      }
    } else {
      res.redirect(routes.user.login+'?redirect_url='+req.url);
    }
  },
  
  updateAddress: function(req, res) {
    if (req.isAuthenticated()) {
      var data = {};
      User.update(req.user._id, req.user._rev,
                  {"address": req.body.address},
                  function(err, data) {
        console.log('res ->', data);
        res.redirect(req.url);
      });
    } else {
      res.redirect(routes.user.login+'?redirect_url='+req.url);
    }
  },
  
  getAddress: function(req, res){
    
  },
  
};