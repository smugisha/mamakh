var passport = require('passport');
var React = require('react');
var User =require('../models/user');
var routes = require('./allRoutes');
var Order = require('../models/order');
var mail = require('../controllers/mailer');

// Export functions
module.exports = {
  
  // Display cart
  getCart: function (req,res) {
    var cart = require('../builds/cart').ShoppingCart;
    var page = React.renderComponentToStaticMarkup(
      cart({
        cart: req.session.cart || {},
        user: (req.user) ? req.user.username : 'Guest',
        checkoutLink: (req.isAuthenticated()) ? 'order?user='+req.user.username : routes.user.login+'?redirect_url='+routes.checkout.order,
        totalPrice: (req.session.cart) ? req.session.cart.total : 0,
      })
    );
    res.send(page);
  },
  
  // Order form checking of order and shipping info
  getCheckout: function (req,res) {
    if (req.isAuthenticated()) {
      var guestForm = require('../builds/guestCheckout').guestForm;
      var page = React.renderComponentToStaticMarkup(
        guestForm({
          loggedIn: req.isAuthenticated(),
          user: req.user.username,
          redirect_url: req.url,
          action: 'order',
        })
      );
      res.send(page);
      
    } else {
      
      // Redirect if logged in
      res.redirect(routes.user.login + '?redirect_url=' + req.url);
    }
  },
  
  // Handle posted order confirmation
  postCheckout: function(req, res) {
    
    var order = {
      createdOn : Date.now(),
      shipping  : {
        customer: req.user.username,
        address : req.user.address,
        deliveryNotes: req.param('deliveryNotes'),
      },
      products  : req.session.cart,
    };
    
    Order.submit(order, function(err, response) {
      if (err) {
        console.log('err-> ', err, response);
      } else {
        
        var products = req.session.cart.products;
        var recipients = Object.keys(products).map(function (item) {
          return products[item].email;
        }).filter( function(elem, pos, self){
          return self.indexOf(elem) == pos;
        });
        
        var orders = recipients.map(function (r) {
          return { email: r, orders: [] };
        });
        
        Object.keys(products).forEach(function(order) {
          orders[recipients.indexOf(products[order].email)]
          .orders.push({
            name  : products[order].name,
            price : products[order].price,
            qty   : products[order].quantity,
          });
        });
        
        orders.forEach(function(order){
          mail.send(order, req.user);
        });
        
        console.log(recipients);
        console.log(orders);
        console.log(req.session.cart);
        
        delete req.session.cart;
        res.redirect(routes.home);
      }
    });
  },
};
