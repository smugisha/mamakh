module.exports = {
  
  home : '/',
  
  user: {
    signup  : '/signup',
    login   : '/login',
    profile : '/profile/users/:userID',
    logout  : '/logout',
    address : '/profile/users/:userID',
  },
  
  products: {
    search  : '/search',
    item    : '/item/:itemID',
  },
  
  cart: {
    addFood    : '/cart/add/food',
    removeFood : '/cart/remove/food',
    updateCart : '/cart/update/food',
  },
  
  // Checkout order
  checkout: {
    cart: '/checkout/cart',
    order: '/checkout/order',
  },
  
};