// shopping cart manager

// ToDo: implement error checks for integers and datatypes

var arango = require('arango');
var dbUrl  = require('../../config/database');
var db = new arango.Connection(dbUrl.url + '/mamakh-test:foods');
var _  = require('underscore');

module.exports = {
  
  // Add product to cart
  addProduct: function(req, res) {
    try {
      // Initalise cart
      if (!req.session.cart) {
        req.session.cart = {
          products: {},
          count: 0,
          total: 0
        };
      }
      
      // Get product from database for given id
      if ( req.body.quantity > 0 ) {
        var item = req.body.id;
        var quantity = req.body.quantity;
        db.document.get(item, function(err, product){
          if(err) {
            console.log("err: %j", product);
          }
          else {

            // Check if product already in cart
            if (!req.session.cart.products[item]) {

              // Add product if not
              req.session.cart.products[item] = {
                id: product._id,
                name: product.name,
                price: product.price,
                seo: product.seo,
                quantity: quantity,
                restaurant: product.restaurantName,
                email: product.email,
              };
            } else {

              // Increment count if already added
              req.session.cart.products[item].quantity = req.session.cart.products[item].quantity + quantity;
            }

            // Total cart
            req.session.cart.count = req.session.cart.count + quantity;
            req.session.cart.total = (parseFloat(req.session.cart.total) + (product.price * quantity)).toFixed(2);
            }
          
          // Respond with rendered cart
          res.json( {cart: req.session.cart} );
        
        });
      }
      else {
        res.json( {cart: req.session.cart} );
      }
        
    } catch(err) {
      console.log(err);
    }
  },
  
  // Remove product from cart
  removeProduct: function(req, res) {
    
    var item = req.body.id;
    
    // Remove product
    if ( req.session.cart ) {
      delete req.session.cart.products[item];

      // Total cart
      req.session.cart.count = 0;
      req.session.cart.total = 0;
      _.each(req.session.cart.products, function (product) {
        req.session.cart.count = req.session.cart.count + product.quantity;
        req.session.cart.total = (parseFloat(req.session.cart.total) + (product.price * product.quantity)).toFixed(2);
      });

      // Remove cart if empty
      if (req.session.cart.count === 0) {
        delete req.session.cart;
        res.json({cart: {
          products: {},
          count: 0,
          total: 0
        }});
      }
      else {
        // Respond with rendered cart
        res.json({cart: req.session.cart});
      }
    }
    else {
      res.json({cart: {
          products: {},
          count: 0,
          total: 0
        }});
    }
  },
  
  // update quantity of product from cart
  updateCart: function(req, res) {
    try {
      if (req.session.cart) {
        // Get product from database for given id
        if ( req.body.quantity > 0 ) {
          var item = req.body.id;
          var quantity = req.body.quantity;
          db.document.get(item, function(err, product){
            if(err) { console.log("err: %j", product); }
            
            // Check if product already in cart
            if (!req.session.cart.products[item]) {
              // Add product if not
              req.session.cart.products[item] = {
                id: product._id,
                name: product.name,
                price: product.price,
                seo: product.seo,
                quantity: quantity
              };
            } else {

              // Increment count if already added
              var difference = req.session.cart.products[item].quantity - quantity;
              req.session.cart.products[item].quantity = quantity;
            }
          
            // Total cart
            req.session.cart.count = req.session.cart.count - difference;
            req.session.cart.total = (parseFloat(req.session.cart.total) - (product.price * difference)).toFixed(2);
          
            // Respond with rendered cart
            res.json( {cart: req.session.cart} );
        
          });
        } else {
          res.json( {cart: req.session.cart} );
        }
      } else {
        res.json( {cart: {
          products: {},
          count: 0,
          total: 0
        }} );
      }
    } catch(err) {
      console.log(err);
    }
  }
};
