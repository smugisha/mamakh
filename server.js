#!/usr/bin/env node
// app server for mamakh
require('newrelic');
var flash    = require('connect-flash');
var passport = require('passport');
var express  = require('express');
var app      = express();
var ejs      = require('ejs');
var port     = process.env.PORT || 3000;

var morgan   = require('morgan');

var favicon      = require('static-favicon');

// session store
var session       = require('express-session');
var RedisStore    = require('connect-redis')(session);
var redisConfig   = require('./config/redis-config');

// database configuration
var arango        = require('arango');
var configDB = require('./config/database.js');
var db = arango.Connection(configDB.url);
console.log(redisConfig);
console.log(new RedisStore(redisConfig));
require('./config/passport')(passport);

// express configuration
app.use(morgan('dev')); // log every request to the console
app.use(require('cookie-parser')()); // read cookies (needed for auth
app.use(require('body-parser')()); // get information from html forms
app.use(require('method-override')());

app.set('views', __dirname + '/app/views');
app.use(express.static(__dirname + '/app/public'));
app.set('view engine', 'ejs'); // set up dotjs for templating

// passport configuration
app.use(session({store: new RedisStore(redisConfig), secret: 'mamakh-cyberrestaurant' }));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(require('errorhandler')({ dumpExceptions: true, showStack: true }));

// routes
require('./app/routes/routes.js')(app, passport); // load our routes and pass in fully configured passport

app.listen(port);
console.log('The Mamakh CyberRestaurant: ' + port);
