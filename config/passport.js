// config/passport.js

var LocalStrategy = require('passport-local').Strategy;

// load user model
var User = require('../app/models/user');

module.exports = function(passport) {
  // passport session setup
  // required for persistent login sessions

  // serializes the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.id||user._id);
  });
  // deserializes the user
  passport.deserializeUser (function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // local signup
  passport.use('local-signup', new LocalStrategy({
    // overide local strategy's use of username and password with
    // username and password
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true // allows to pass back the entire request to callback
  }, function(req, username, password, done) {
    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(function(){
      // find a user whose username is the same as the forms username
      // check to see if user is trying to login already exists in the database
      User.findOne({ 'username': username }, function(err, res) {
        // if any errors, return error
        if (err) {
          return done(err);
        }
        // if there is a user with that username
        if (res.result[0]) {
          return done(null, false, req.flash('signupMessage', 'That username is already taken'));
        } else {
          // if there is no user with that username
          // create the user
          User.create(username, password, function(err, newUser){
            return done(null, newUser);
          });
        }
      });
    });
  }));

  // local login
  passport.use('local-login', new LocalStrategy({
    // overide local strategy's use of username and password with
    // username and password
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true // allows to pass back the entire request to callback
  }, function(req, username, password, done) {
    process.nextTick(function () {
      // callback with username and passwordfrom the form
      // find a user whose username is the same as the forms username
      // we are checking to see if the user trying to login already exists
      User.findOne({ 'username': username }, function(err, res) {
        // if there are any errors, return the error before anything else
        if(err) { return done(err); }
        // if no user is found, return the message
        if (!res.result[0]) {
          return done(null, false, req.flash('loginMessage', 'No user found!'));
        } 
        User.verifyPassword(res.result[0].password, password, function(ret) {
          if (!ret){
            // if the user is found but the password is wrong
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
            // create loginMessage and save it to session as flashdata
          }
          else {
            // all is well, return successful user
            return done(null, res.result[0]);
          }
        });
      });
    });
  }));
};
