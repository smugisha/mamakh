module.exports = {
  host: 'mamak-redis.qafne6.0001.apse1.cache.amazonaws.com',
  port: 6379,
  ttl: (5*3600), // 5 hours
  client: require('redis').createClient('mamak-redis.qafne6.0001.apse1.cache.amazonaws.com')
};
